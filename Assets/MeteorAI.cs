using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class MeteorAI : MonoBehaviour
{
    private Transform playerTransform;
    private Rigidbody2D _rigidbody2D;

    [SerializeField] private float _power = 5f;
  

    private void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        _rigidbody2D = gameObject.GetComponent<Rigidbody2D>() as Rigidbody2D;
    }

    private void FixedUpdate()
    {
        Vector2 playerPosition = playerTransform.position;
        Vector2 myPosition = transform.position;

        Vector2 heading = playerPosition - myPosition;

        _rigidbody2D.AddForce(heading, ForceMode2D.Force);

        if (_rigidbody2D.velocity.magnitude > _power)
        {
            _rigidbody2D.velocity = _rigidbody2D.velocity.normalized * _power;
        }
    }
}