using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;


public class AddRandomInitialVelocity : MonoBehaviour
{
    [SerializeField] private Vector2Int _VelocityMagnitude = new Vector2Int(25, 500);
  

    private void Start()
    {
        Vector2 vel = new Vector2(Random.Range(_VelocityMagnitude.x / 100f, _VelocityMagnitude.y / 100f), Random.Range(
            _VelocityMagnitude.x / 100f, _VelocityMagnitude.y / 100f));
        gameObject.GetComponent<Rigidbody2D>().velocity = vel;
    }
}