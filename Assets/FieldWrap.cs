using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class FieldWrap : MonoBehaviour
{
    private int _leftConstraint = -100;
    private int _rightConstraint = 100;
    private int _bottomConstraint = -100;
    private int _topConstraint = 100;

    [SerializeField] private Vector2Int _worldBound = new Vector2Int(100, 100);

    private void Start()
    {
        _leftConstraint = -_worldBound.x;
        _rightConstraint = _worldBound.x;
        _bottomConstraint = -_worldBound.y;
        _topConstraint = _worldBound.y;
    }

    private void FixedUpdate()
    {
        if (transform.position.x < _leftConstraint - 10)
        {
            transform.position = new Vector3(_rightConstraint, transform.position.y, transform.position.z);
        }

        if (transform.position.x > _rightConstraint + 10)
        {
            transform.position = new Vector3(_leftConstraint, transform.position.y, transform.position.z);
        }

        if (transform.position.y < _bottomConstraint - 10)
        {
            transform.position = new Vector3(transform.position.x, _topConstraint, transform.position.z);
        }

        if (transform.position.y > _topConstraint + 10)
        {
            transform.position = new Vector3(transform.position.x, _bottomConstraint, transform.position.z);
        }
    }
}