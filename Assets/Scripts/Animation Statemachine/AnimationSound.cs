﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSound : StateMachineBehaviour
{
    [SerializeField] private AudioClip thrusterSound;

    

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<AudioSource>().clip = thrusterSound;
        animator.gameObject.GetComponent<AudioSource>().Play();
    }


    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    //public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
   // {
   //     animator.gameObject.GetComponent<AudioSource>().Stop();
   // }
}