﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletThrust : StateMachineBehaviour
{

    [SerializeField] private float thrust;
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(thrust, 0f), ForceMode2D.Impulse);
    }

  
}
