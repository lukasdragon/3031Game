﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

namespace Menu
{
    public class mainMenu : MonoBehaviour
    {
      

        [SerializeField] private GameObject mainPanel;
        [SerializeField] private GameObject aboutPanel;
        [SerializeField] private GameObject settingsPanel;
      


        public void start_Button(int ID)
        {
            SceneManager.LoadScene(ID);
        }

        public void about_Button()
        {
            mainPanel.SetActive(false);
            aboutPanel.SetActive(true);
        }

        public void about_back_button()
        {
            aboutPanel.SetActive(false);
            mainPanel.SetActive(true);
        }

        public void settings_Button()
        {
            mainPanel.SetActive(false);
            settingsPanel.SetActive(true);
        }

      


        public void settings_back_button()
        {
            settingsPanel.SetActive(false);
            mainPanel.SetActive(true);
            
        }

        public void quit_Button()
        {
            Application.Quit();
        }
    }
}