﻿using System.Collections;
using SO_Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace menu
{
    [RequireComponent(typeof(AudioSource))]
    public class UIHelper : MonoBehaviour
    {
        [SerializeField] private IntReference _playerHealth;
        [SerializeField] private IntReference _maxPlayerHealth;
        [SerializeField] private FloatReference _playerFuel;
        [SerializeField] private FloatReference _maxPlayerFuel;

        [SerializeField] private Image _healthBar;

        [SerializeField] private Image _fuelBar;

        [SerializeField] private TMP_Text _waveTimer;

        [SerializeField] private TMP_Text _winText;

        [SerializeField] private FloatReference _waveTime;


        [Header("Sound files")] [SerializeField]
        private AudioClip[] _numberVoice;


        [SerializeField] private AudioSource _as;


        public void Awake()
        {
            UpdatePlayerHealth();
            UpdateFuelBar();


            _as = GetComponent<AudioSource>();
        }

        public void WaveCoolDown()
        {
            _waveTimer.text = ((float)_waveTime).ToString("N2").Replace("0", "o");
            _waveTimer.enabled = true;
            StartCoroutine(PlaySoundEvery(1.0f, (int)_waveTime));
        }

        public void PlayerWin()
        {
            _winText.enabled = true;
        }

        private void Update()
        {
            if (!_waveTimer.enabled) return;

            if (_waveTime < 0.25f)
            {
                _waveTimer.enabled = false;
            }

            _waveTimer.text = ((float)_waveTime).ToString("N2").Replace("0", "o");
            UpdateFuelBar();
        }


        IEnumerator PlaySoundEvery(float t, int times)
        {
            for (var i = times; i > 0; i--)
            {
                _as.PlayOneShot(_numberVoice[i-1]);
                yield return new WaitForSeconds(t);
            }

            // GetComponent<AudioSource>().PlayOneShot(roundsUpSound);
        }


        public void UpdatePlayerHealth()
        {
            _healthBar.fillAmount = ((float)_playerHealth / (float)_maxPlayerHealth);
        }

        public void UpdateFuelBar()
        {
            _fuelBar.fillAmount = (float)_playerFuel / _maxPlayerFuel;
        }
    }
}