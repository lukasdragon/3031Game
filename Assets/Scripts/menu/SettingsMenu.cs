﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace Menu
{
    public class SettingsMenu : MonoBehaviour
    {
        public AudioMixer audioMixer;

        [SerializeField] private Button defaultSelection;

        Resolution[] _resolutions;

        public TMP_Dropdown resolutionDropdown;
        public Toggle fullScreenToggle;
        public Slider musicSlider;
        public Slider ambienceSlider;
        public Slider effectSlider;
        public TMP_Dropdown qualityDropdown;


        void Start()
        {
            _resolutions = Screen.resolutions;

            resolutionDropdown.ClearOptions();

            var options = new List<string>();

            var currentResolutionIndex = 0;
            for (var i = 0; i < _resolutions.Length; i++)
            {
                var option = _resolutions[i].width + " x " + _resolutions[i].height + " " + _resolutions[i].refreshRate + "HZ";
                options.Add(option);

                if (_resolutions[i].Equals(Screen.currentResolution))
                {
                    currentResolutionIndex = i;
                }
            }

            resolutionDropdown.AddOptions(options);
            resolutionDropdown.value = currentResolutionIndex;
            resolutionDropdown.RefreshShownValue();

            LoadSettings();
        }

        public void selectDefault()
        {
            defaultSelection.Select();
        }

        void LoadSettings()
        {
            var effectVolume = PlayerPrefs.GetFloat("EffectsVolume");
            var musicVolume = PlayerPrefs.GetFloat("MusicVolume");
            var ambienceVolume = PlayerPrefs.GetFloat("AmbienceVolume");
            var quality = PlayerPrefs.GetInt("Quality");
            var fullScreen = PlayerPrefs.GetInt("isFullScreen") != 0;
            SetEffectsVolume(effectVolume);
            effectSlider.value = effectVolume;
            SetMusicVolume(musicVolume);
            musicSlider.value = musicVolume;
            SetAmbienceVolume(ambienceVolume);
            ambienceSlider.value = ambienceVolume;
            SetFullscreen(fullScreen);
            fullScreenToggle.isOn = fullScreen;
            SetQuality(quality);
            qualityDropdown.value = quality;
            qualityDropdown.RefreshShownValue();
        }

        public void SetEffectsVolume(float volume)
        {
            audioMixer.SetFloat("EffectsVolume", volume);
            PlayerPrefs.SetFloat("EffectsVolume", volume);
            PlayerPrefs.Save();
        }

        public void SetAmbienceVolume(float volume)
        {
            audioMixer.SetFloat("AmbienceVolume", volume);
            PlayerPrefs.SetFloat("AmbienceVolume", volume);
            PlayerPrefs.Save();
        }

        public void SetMusicVolume(float volume)
        {
            audioMixer.SetFloat("MusicVolume", volume);
            PlayerPrefs.SetFloat("MusicVolume", volume);
            PlayerPrefs.Save();
        }

        public void SetQuality(int qualityIndex)
        {
            QualitySettings.SetQualityLevel(qualityIndex);
            PlayerPrefs.SetInt("Quality", qualityIndex);
            PlayerPrefs.Save();
        }

        public void SetFullscreen(bool isFullScreen)
        {
            Screen.fullScreen = fullScreenToggle.isOn;
            PlayerPrefs.SetInt("isFullScreen", (isFullScreen ? 1 : 0));

            PlayerPrefs.Save();
        }

        public void SetResolution(int resolutionIndex)
        {
            Resolution resolution = _resolutions[resolutionIndex];
            Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        }
    }
}