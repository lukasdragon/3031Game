﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class SplashVideo : MonoBehaviour
{


    [SerializeField] private VideoClip clip;
    [SerializeField] private Camera camera;
    
    void Start()
    {
        // VideoPlayer automatically targets the camera backplane when it is added
        // to a camera object, no need to change videoPlayer.targetCamera.
        var videoPlayer = camera.gameObject.AddComponent<UnityEngine.Video.VideoPlayer>();

        // Play on awake defaults to true. Set it to false to avoid the url set
        // below to auto-start playback since we're in Start().
        videoPlayer.playOnAwake = false;

        // By default, VideoPlayers added to a camera will use the far plane.
        // Let's target the near plane instead.
        videoPlayer.renderMode = UnityEngine.Video.VideoRenderMode.CameraNearPlane;

        // This will cause our Scene to be visible through the video being played.
        videoPlayer.targetCameraAlpha = 0.5F;

        // Set the video to play. URL supports local absolute or relative paths.
        // Here, using absolute.
        videoPlayer.clip = clip;
        

        // Restart from beginning when done.
        videoPlayer.isLooping = false;
        
        videoPlayer.Prepare();

        // Each time we reach the end, we slow down the playback by a factor of 10.
        videoPlayer.loopPointReached += EndReached;
        
        videoPlayer.prepareCompleted += PrepareCompleted;

    }

    void EndReached(UnityEngine.Video.VideoPlayer vp)
    {
        SceneManager.LoadScene(1);
    }
    
    
    void PrepareCompleted(VideoPlayer vp) {
        vp.Play();
    }
}
