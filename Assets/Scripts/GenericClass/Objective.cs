﻿using JetBrains.Annotations;
using SO_Utilities;
using UnityEngine;
using UnityEngine.UIElements;

public abstract class Objective : MonoBehaviour
{
    [Header("Settings")] 
    [UsedImplicitly] [SerializeField] protected GameEvent loseObjective;
    [SerializeField] [UsedImplicitly] protected GameEvent winObjective;


    [UsedImplicitly]
    public abstract string GetName();

    [UsedImplicitly]
    public abstract bool PlayerFriendly();

    [UsedImplicitly]
    public abstract bool HealthBar();

    [UsedImplicitly]
    public abstract void OnDeath();
}