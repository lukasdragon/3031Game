﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Enemy : ScriptableObject
{
  [SerializeField] private string _enemyName;
  [SerializeField] private GameObject[] prefabVariants;
 

  public string enemyName
  {
    get { return _enemyName; }
  }


  public GameObject GetEnemyPrefab()
  {
    return prefabVariants[Random.Range(0, prefabVariants.Length)];
  }
}
