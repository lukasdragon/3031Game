﻿using System;
using UnityEngine;

[Serializable]
public class Wave : object
{
    public string waveName;
    public float spawnDelay = 10f;
    public int waveValue = 100;

    public float minBuffer = 3f;
    public float maxBuffer = 5f;

    public WavePair[] enemies;
}


[Serializable]
public class WavePair
{
    public float spawnChance = 100f;
    public int count = 1;
    public Enemy enemy;
}