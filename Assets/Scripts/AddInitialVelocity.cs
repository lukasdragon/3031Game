﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class AddInitialVelocity : MonoBehaviour
{

    [SerializeField] private Vector2 initialForce;
    
    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Rigidbody2D>().velocity = initialForce;
    }
}
