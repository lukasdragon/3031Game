﻿using UnityEngine;

namespace SO_Utilities
{
    [CreateAssetMenu(menuName = "Set/Rigidbody2D")]
    public class Rigidbody2dSet : RuntimeSet<Rigidbody2D>{}
}
