﻿using UnityEngine;

namespace LevelObjective
{
    public class ObjectiveDefend : Objective
    {
        public override string GetName()
        {
            return "Defend";
        }

        public override bool PlayerFriendly()
        {
            return true;
        }

        public override bool HealthBar()
        {
            return true;
        }

        public override void OnDeath()
        {
            loseObjective.Raise(); 
        }
    }
}
