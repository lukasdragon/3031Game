﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

[RequireComponent(typeof(ParticleSystem))]
public class Starfield : MonoBehaviour
{
    [SerializeField] private int maxStars = 100;
    [SerializeField] private float starSize = 0.1f;
    [SerializeField] private float starSizeRange = 0.5f;
    [SerializeField] private float fieldWidth = 20f;
    [SerializeField] private float fieldHeight = 25f;
    [SerializeField] private bool colorize;


    [SerializeField] private float parallaxFactor = 0f;

    private ParticleSystem _particles;
    private ParticleSystem.Particle[] _stars;


    private float _xOffset;
    private float _yOffset;

    Transform _camera;


    private void Awake()
    {
        _camera = Camera.main.transform;


        _stars = new ParticleSystem.Particle[maxStars];
        _particles = GetComponent<ParticleSystem>();

        Assert.IsNotNull(_particles, "Particle system missing from object!");

        _xOffset = fieldWidth * 0.5f; // Offset the coordinates to distribute the spread
        _yOffset = fieldHeight * 0.5f; // around the object's center

        for (var i = 0; i < maxStars; i++)
        {
            var randSize = Random.Range(starSizeRange, starSizeRange + 1f); // Randomize star size within parameters
            var scaledColor =
                (true == colorize) ? randSize - starSizeRange : 1f; // If coloration is desired, color based on size

            _stars[i].position = GetRandomInRectangle(fieldWidth, fieldHeight) + transform.position;
            _stars[i].startSize = starSize * randSize;
            _stars[i].startColor = new Color(1f, scaledColor, scaledColor, 1f);
        }

        _particles.SetParticles(_stars, _stars.Length); // Write data to the particle system
    }


    private void Update()
    {
        for (int i = 0; i < maxStars; i++)
        {
            Vector3 pos = _stars[i].position + transform.position;

            if (pos.x < (_camera.position.x - _xOffset))
            {
                pos.x += fieldWidth;
            }
            else if (pos.x > (_camera.position.x + _xOffset))
            {
                pos.x -= fieldWidth;
            }

            if (pos.y < (_camera.position.y - _yOffset))
            {
                pos.y += fieldHeight;
            }
            else if (pos.y > (_camera.position.y + _yOffset))
            {
                pos.y -= fieldHeight;
            }

            _stars[i].position = pos - transform.position;
        }

        _particles.SetParticles(_stars, _stars.Length);


        var newPos = _camera.position * parallaxFactor; // Calculate the position of the object
        newPos.z = 0; // Force Z-axis to zero, since we're in 2D
        transform.position = newPos;
    }


    // GetRandomInRectangle
    //----------------------------------------------------------
    // Get a random value within a certain rectangle area
    //
    private Vector3 GetRandomInRectangle(float width, float height)
    {
        var x = Random.Range(0, width);
        var y = Random.Range(0, height);
        return new Vector3(x - _xOffset, y - _yOffset, 0);
    }
}