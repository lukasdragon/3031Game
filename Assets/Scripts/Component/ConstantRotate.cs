﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class ConstantRotate : MonoBehaviour
{
    [SerializeField] private float degrees;
    [SerializeField] private float degreeVariance;
    [SerializeField] private bool randomizeDirection;
    private Rigidbody2D _rb;

    private void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();

        degreeVariance = Random.Range(0, degreeVariance);
        if (Random.value <= 0.5)
            degreeVariance *= -1;
        degrees += degreeVariance;
        if (!randomizeDirection) return;
        if (Random.value <= 0.5)
            degrees *= -1;
    }

    private void FixedUpdate()
    {
        _rb.MoveRotation(_rb.rotation + degrees);
    }
}