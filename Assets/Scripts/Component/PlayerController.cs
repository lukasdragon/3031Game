﻿using System;
using System.Collections;
using JetBrains.Annotations;
using SO_Utilities;
using UnityEngine;
using UnityEngine.Serialization;
using Vector2 = UnityEngine.Vector2;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BulletShooter))]
[RequireComponent(typeof(Health))]
//[RequireComponent(typeof(AudioSource))]
public class PlayerController : MonoBehaviour
{
    [SerializeField] private float thrust = 1f;
    [SerializeField] private float turnThrust = 4f;
    [SerializeField] private float maxVelocity = 10f;
    [SerializeField] private Animator thrusterAnimator;


    [SerializeField] private FloatVariable _fuel;
    [SerializeField] private FloatVariable _maxFuel;


    private Health _health;
    private bool _inertialDampeners;

    private Animator _shipAnimator;
    private Rigidbody2D _rb;
    private Collider2D _collider;
    private BulletShooter _gun;


    private bool _dead;

    private static readonly int Thrust = Animator.StringToHash("Thrust");
    private static readonly int Velocity = Animator.StringToHash("Velocity");
    private static readonly int Explode = Animator.StringToHash("Explode");
    private static readonly int RespawnID = Animator.StringToHash("Respawn");


    private void Start()
    {
        _gun = gameObject.GetComponent<BulletShooter>();
        _shipAnimator = gameObject.GetComponent<Animator>();
        _rb = gameObject.GetComponent<Rigidbody2D>();
        _collider = gameObject.GetComponent<Collider2D>();
        _health = gameObject.GetComponent<Health>();

        _fuel.SetValue(_maxFuel);
    }

    private void Update()
    {
        if (_dead) return;
        //Sends velocity to the thruster animator
        thrusterAnimator.SetFloat(Velocity, _rb.velocity.magnitude);

        //Toggles inertial dampeners
        if (Input.GetButtonDown("InertialDampeners"))
        {
            _inertialDampeners = !_inertialDampeners;
        }

        //Sets animation states
        if (_fuel.value > 0)
        {
            if (Input.GetButton("Thruster"))
            {
                thrusterAnimator.SetFloat(Thrust, 1f);
            }
            else if (_inertialDampeners && _rb.velocity.magnitude > 0.1f)
            {
                thrusterAnimator.SetFloat(Thrust, 0.5f);
            }
            else
            {
                thrusterAnimator.SetFloat(Thrust, 0f);
            }
        }
        else
        {
            thrusterAnimator.SetFloat(Thrust,0f);
        }

        if (Input.GetButton("Fire1"))
        {
            _gun.Fire();
        }
    }

    private void FixedUpdate()
    {
        var force = new Vector2(thrust, 0);

        //Ensures that the ship doesn't go above the max velocity
        _rb.velocity = Vector2.ClampMagnitude(_rb.velocity, maxVelocity);
        if (_rb.velocity.magnitude > maxVelocity * 3)
            _health.DealDamage((int)(_rb.velocity.magnitude - maxVelocity));

        if (!(_health.GetHealth() > 0)) return;
        //Adds torque to rotate the ship
        var torque = Input.GetAxis("Horizontal") * turnThrust;
        _rb.AddTorque(torque);

       
        if (_fuel.value > 0)
        {
            //Adds thrust when thrust button is pressed. Otherwise enables inertial dampeners
            //If they have been enabled
            if (Input.GetButton("Thruster"))
            {
                _rb.AddRelativeForce(force, ForceMode2D.Force);
                _fuel.SetValue(_fuel.value - (force.magnitude * Time.fixedDeltaTime));
            }
            else if (_inertialDampeners && _rb.velocity.magnitude > 0.1f)
            {
                _rb.AddForce(-(_rb.velocity * 10f), ForceMode2D.Force);
                _fuel.SetValue(_fuel.value - (_rb.velocity.magnitude * (10f * Time.fixedDeltaTime)));
            }
        }
    }

    [UsedImplicitly]
    public void Die()
    {
        _dead = true;
        _shipAnimator.SetTrigger(Explode);
        _collider.enabled = false;
        thrusterAnimator.SetFloat(Velocity, 0);
        thrusterAnimator.SetFloat(Thrust, 0);
    }


    [UsedImplicitly]
    public void Respawn()
    {
        _fuel.SetValue(_maxFuel);
        _shipAnimator.SetTrigger(RespawnID);
        _dead = false;
        _collider.enabled = true;
        
    }
}