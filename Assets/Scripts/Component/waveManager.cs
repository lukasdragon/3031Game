﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using SO_Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

public class waveManager : MonoBehaviour
{
    [SerializeField] private Wave[] level;
    [SerializeField] private float timeBetweenWaves = 5f;


    [SerializeField] private FloatReference timer;


    private Camera _camera;


    private float _distanceZ;


    private int _leftConstraint = -100;
    private int _rightConstraint = 100;
    private int _bottomConstraint = -100;
    private int _topConstraint = 100;

    [SerializeField] private Vector2Int _worldBound = new Vector2Int(100, 100);


    [SerializeField] private GameEvent onWaveChange;
    [SerializeField] private GameEvent onLevelComplete;
    [SerializeField] private IntReference waveIndex;


    private void Start()
    {
        _leftConstraint = -_worldBound.x;
        _rightConstraint = _worldBound.x;
        _bottomConstraint = -_worldBound.y;
        _topConstraint = _worldBound.y;


        waveIndex.Value = 0;
    }


    private int _enemyIndex, _spawnCountIndex = 0;
    private bool _levelDone = false;

    private void Update()
    {
        if (_levelDone)
        {
            waveIndex.Value = 0;
            timer.Value = 5;
            _levelDone = false;
        }

        if (timer < 0f)
        {
            //Checks if we've beaten the level or not
            if (waveIndex >= level.Length)
            {
                _levelDone = true;
                waveIndex.Value = 0;
                if (onLevelComplete != null)
                    onLevelComplete.Raise();
                //We probably want to break out this or something...
            }


            //Checks if we've spawned enough of an enemy yet. If so we go to the next enemy
            if (_spawnCountIndex >= level[waveIndex].enemies[_enemyIndex].count)
            {
                _enemyIndex += 1;
                _spawnCountIndex = 0;
            }


            //Checks if our current enemy index is valid.... If not, goes to the next wave
            if (_enemyIndex >= level[waveIndex].enemies.Length)
            {
                _enemyIndex = 0;
                waveIndex.Value += 1;
                timer.Value = timeBetweenWaves;
                //  Debug.Log(level[waveIndex].waveName);
                if (onWaveChange != null)
                    onWaveChange.Raise();
                return;
            }


            //    Debug.Log(_waveIndex + " wave Index|" + _enemyIndex + " Enemy Index|" + _spawnCountIndex +
            //            " SpawnCount Index");
            _spawnCountIndex += 1;
            if (Random.Range(0, 100) <= level[waveIndex].enemies[_enemyIndex].spawnChance)
            {
                //Resets the timer if we successfully spawn an enemy!
                timer.Value = level[waveIndex].spawnDelay;

                //Spawn enemy
                var spawnPos = Vector3.zero;


                var x = Random.Range(0, 4);
                switch (x)
                {
                    case 0:
                        spawnPos = new Vector3(Random.Range(_leftConstraint, _rightConstraint),
                            _topConstraint,
                            0f);
                        break;
                    case 1:
                        spawnPos = new Vector3(_rightConstraint,
                            Random.Range(_bottomConstraint, _topConstraint),
                            0f);
                        break;
                    case 2:
                        spawnPos = new Vector3(Random.Range(_leftConstraint, _rightConstraint),
                            _bottomConstraint,
                            0f);
                        break;
                    case 3:
                        spawnPos = new Vector3(_leftConstraint,
                            Random.Range(_bottomConstraint, _topConstraint),
                            0f);
                        break;
                    default:
                        // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
                        Debug.Log("Random number in waveManager did not match a case...");
                        break;
                }

                if (!_levelDone)
                    Instantiate(level[waveIndex].enemies[_enemyIndex].enemy.GetEnemyPrefab(), spawnPos,
                        transform.rotation);
            }
        }

        //Decrease the timer
        timer.Value -= Time.deltaTime;
    }
}