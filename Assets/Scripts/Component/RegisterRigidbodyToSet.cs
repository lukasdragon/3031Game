﻿using System.Collections;
using System.Collections.Generic;
using SO_Utilities;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class RegisterRigidbodyToSet : MonoBehaviour
{
    public Rigidbody2dSet runtimeSet;
    private void OnEnable()
    {
        runtimeSet.Add(GetComponent<Rigidbody2D>());
    }

    private void OnDisable()
    {
        runtimeSet.Remove(GetComponent<Rigidbody2D>());
    }
}