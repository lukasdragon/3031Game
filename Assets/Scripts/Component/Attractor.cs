﻿using System;
using System.Collections;
using System.Collections.Generic;
using SO_Utilities;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    //Default 667.4f
    [SerializeField] private float G = 667.4f;

   [SerializeField] private Rigidbody2dSet rigidbodiesToAttract;

    private Rigidbody2D _rb;


    private void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
    }


    private void FixedUpdate()
    {
        foreach (var rbToAttract in rigidbodiesToAttract.items)
        {
            if (rbToAttract.gameObject != gameObject)
                Attract(rbToAttract);
        }
    }

    private void Attract(Rigidbody2D rbToAttract)
    {
        var direction = _rb.position - rbToAttract.position;
        var distance = direction.sqrMagnitude;

        if (Math.Abs(distance) < 0.01)
            return;

        var forceMagnitude = G * (_rb.mass * rbToAttract.mass) / distance;
        var force = direction.normalized * forceMagnitude;

        rbToAttract.AddForce(force);
    }
}