﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kill : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D other)
    {
        var hlth = other.gameObject.GetComponent<Health>();
        if (hlth != null)
        {
            hlth.DealDamage(1000000);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        var hlth = other.gameObject.GetComponent<Health>();
        if (hlth != null)
        {
            hlth.DealDamage(1000000);
        }
    }
}
