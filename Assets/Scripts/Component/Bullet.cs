﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
public class Bullet : MonoBehaviour
{
    [SerializeField] private float explosionTime;
    [SerializeField] private float explosionAnimationLength = 0.9f;
    [SerializeField] private int explosionDamage = 10;
    [SerializeField] private float primeTime = 0.5f;
    [SerializeField] private UnityEvent Onexplode;
    [HideInInspector] public string shooterTag = "Player";
    
    
    
    
    private Animator _animator;
    private Rigidbody2D _rb;
    private Collider2D _collider;
    private float _explosionTimer;
    private bool _primed;
    private static readonly int ExplodeTrigger = Animator.StringToHash("Explode");

    private void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
        _animator = gameObject.GetComponent<Animator>();
        _collider = gameObject.GetComponent<Collider2D>();
    }

    private void Update()
    {
        _explosionTimer += Time.deltaTime;
        if (!_primed && _explosionTimer > primeTime)
        {
            _primed = true;
        }

        if (!(_explosionTimer > explosionTime)) return;
        Explode();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!_primed && other.CompareTag(shooterTag) || other.CompareTag("Ammo")) return;
        var health = other.GetComponent(typeof(Health)) as Health;
        if (health != null)
        {
            health.DealDamage(explosionDamage);
        }

        Explode();

        var otherRb = other.GetComponent<Rigidbody2D>();
        _rb.velocity = otherRb != null ? other.GetComponent<Rigidbody2D>().velocity : Vector2.zero;




    }

    private void Explode()
    {
        _collider.enabled = false;
        _animator.SetTrigger(ExplodeTrigger);
        Onexplode.Invoke();
        StartCoroutine(Cleanup());
    }


    private IEnumerator Cleanup()
    {
        yield return new WaitForSeconds(explosionAnimationLength);
        Destroy(gameObject);
    }
}