﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class BulletShooter : MonoBehaviour
{
    [SerializeField] private GameObject ammo;
    [SerializeField] private GameObject[] guns;
    [SerializeField] private float shotsPerMinute;
    private float _fireRate;
    private float _cooldown;
    private Rigidbody2D _rb;

    private void Start()
    {
        _rb = gameObject.GetComponent(typeof(Rigidbody2D)) as Rigidbody2D;
        _fireRate = 60 / shotsPerMinute;
        _cooldown = _fireRate;
    }

    private void Update()
    {
        _cooldown += Time.deltaTime;
    }

    public void Fire()
    {
        if (!(_cooldown >= _fireRate)) return;
        foreach (var gun in guns)
        {
            var shot = Instantiate(ammo, gun.transform.position, gun.transform.rotation);
           
            Vector2 vel = new Vector2(_rb.velocity.x,0);
            shot.gameObject.GetComponent<Rigidbody2D>().velocity = vel;
            shot.gameObject.GetComponent<Bullet>().shooterTag = gameObject.tag;
        }

        _cooldown = 0f;
    }
}