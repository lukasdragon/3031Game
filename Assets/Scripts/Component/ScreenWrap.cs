﻿//Scales to screen

using UnityEngine;
using System.Collections;

public class ScreenWrap : MonoBehaviour
{
    private float _leftConstraint = Screen.width;
    private float _rightConstraint = Screen.width;
    private float _bottomConstraint = Screen.height;
    private float _topConstraint = Screen.height;
    [SerializeField] private float buffer = 1.0f;
    private Camera _cam;
    private float _distanceZ;

    private void Start()
    {
        _cam = Camera.main;
        if (_cam == null)
        {
            Debug.LogWarning("No main camera found...");
            return;
        }

        _distanceZ = Mathf.Abs(_cam.transform.position.z + transform.position.z);

        _leftConstraint = _cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, _distanceZ)).x;
        _rightConstraint = _cam.ScreenToWorldPoint(new Vector3(Screen.width, 0.0f, _distanceZ)).x;
        _bottomConstraint = _cam.ScreenToWorldPoint(new Vector3(0.0f, 0.0f, _distanceZ)).y;
        _topConstraint = _cam.ScreenToWorldPoint(new Vector3(0.0f, Screen.height, _distanceZ)).y;
    }

    private void FixedUpdate()
    {
        if (transform.position.x < _leftConstraint - buffer)
        {
            transform.position = new Vector3(_rightConstraint + buffer, transform.position.y, transform.position.z);
        }

        if (transform.position.x > _rightConstraint + buffer)
        {
            transform.position = new Vector3(_leftConstraint - buffer, transform.position.y, transform.position.z);
        }

        if (transform.position.y < _bottomConstraint - buffer)
        {
            transform.position = new Vector3(transform.position.x, _topConstraint + buffer, transform.position.z);
        }

        if (transform.position.y > _topConstraint + buffer)
        {
            transform.position = new Vector3(transform.position.x, _bottomConstraint - buffer, transform.position.z);
        }
    }
}