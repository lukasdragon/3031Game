﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedScroll : MonoBehaviour
{
    public float theScrollSpeed = 0.025f;

    private Transform _camera;

    private void Start()
    {
        if (Camera.main != null) _camera = Camera.main.transform;
    }

    private void Update()
    {
        var position = _camera.position;
        position =
            new Vector3(position.x, position.y + theScrollSpeed, position.z);
        _camera.position = position;
    }
}