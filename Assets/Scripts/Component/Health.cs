﻿using System.Collections;
using System.Linq;
using SO_Utilities;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D))]
public class Health : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private IntReference maxHealth;
    [SerializeField] private IntReference health;
    
    
    [Header("Respawn")]
    [SerializeField] private bool destroyOnDeath = true;
    [SerializeField] private FloatReference respawnLength;
    [SerializeField] private GameObject[] spawnOnDeath;
    
    [Header("GameEvents")]
    [SerializeField] private GameEvent onDeath;
    [SerializeField] private GameEvent onHurt;
    [SerializeField] private GameEvent onRespawn;


    [Header("Impact")]
    [SerializeField] private bool dieOnImpact;
    [SerializeField] private bool takeImpactDmg;
    [SerializeField] private bool onlyRigidbodyImpact;
    [SerializeField] private string[] impactFilterTag;


    private Rigidbody2D _rb;
    private Vector2 _spawnLocation;
    private float _spawnRotation;

    private bool _dead;


    private void Start()
    {
        _rb = gameObject.GetComponent<Rigidbody2D>();
        health.Value = maxHealth.Value;

        _spawnLocation = _rb.position;
        _spawnRotation = _rb.rotation;

        if (dieOnImpact)
            takeImpactDmg = dieOnImpact;
    }

    public void DealDamage(int damage)
    {
        if (_dead) return;
        
        health.Value -= damage;
        
        
        if (onHurt != null)
            onHurt.Raise();


        if (health.Value <= 0)
            Death();
    }

    public int GetHealth()
    {
        return health.Value;
    }

    public int GetMaxHealth()
    {
        return maxHealth.Value;
    }


    private void Death()
    {
        if (_dead) return;
        if (onDeath != null)
            onDeath.Raise();

        foreach (var obj in spawnOnDeath)
        {
            var transform1 = transform;
            Instantiate(obj, transform1.position, transform1.rotation);
        }

        if (destroyOnDeath)
        {
            Destroy(gameObject);
        }
        else
        {
            StartCoroutine(Respawn());
        }
    }

    public void SetMaxHealth()
    {
        health.Value = maxHealth.Value;
        if (onHurt != null)
            onHurt.Raise();
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        //make sure we take impact damage from this object
        if (!takeImpactDmg)
            return;
        if (impactFilterTag.Any(tag => other.transform.CompareTag(tag)))
        {
            return;
        }

        if (onlyRigidbodyImpact && !other.rigidbody)
            return;
        
        if (dieOnImpact)
        {
            DealDamage(health);
            return;
        }

        int hitForce;
        //calculate damage
        if (other.rigidbody)
            hitForce = (int) (other.rigidbody.velocity.magnitude / 2 * other.rigidbody.mass);
        else
            hitForce = (int) other.relativeVelocity.magnitude / 6;

        DealDamage(hitForce);
    }


    private IEnumerator Respawn()
    {
        yield return new WaitForSeconds(respawnLength.Value);
        _rb.velocity = Vector2.zero;
        _rb.MovePosition(_spawnLocation);
        _rb.rotation = _spawnRotation;

        
        
        if (onRespawn != null)
            onRespawn.Raise();
        SetMaxHealth();
        _dead = false;

    }
}